/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package event;

/**
 *
 * @author dell
 */
import java.util.*;
import java.io.*;
public class SentenceDistance {
    public static void main(String args[]) throws FileNotFoundException, IOException
    {
        String currentid=null,refid=null;
        int currentidint=0,refidint=0,postreferents=0;
        String dirName = "annotated\\";
        File f1 = new File(dirName);
        File[] filelist=f1.listFiles();
        HashMap<Integer,Integer> dis=new HashMap<Integer,Integer>();//stores sentence distance as key and corresponding count as the object
        ArrayList<String> verbs=new ArrayList<String>();//stores the name of all the verb chunks in a sentence
        for(int i=0;i<filelist.length;i++)
        {
            File file=filelist[i];
            FileReader fr=new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String   newline=br.readLine();
            while(newline!=null)
            {
                if(newline.contains("<Sentence id="))
            {
                verbs=new ArrayList<String>();
                int ind=newline.indexOf("<Sentence id=")+14;
                currentid=newline.substring(ind,newline.indexOf('\"', ind));
                currentidint=Integer.parseInt(currentid);//retrieve the sentence id of the current sentence
            }
                      if(newline.contains("(("))
            {

                Getvalues gv=new Getvalues();
                String tag=gv.getTag(newline);
                if(tag.contains("VG") && !tag.contains("NULL"))//if the chunk is a verb chunk
                {
                    String name=gv.getName(newline);
                    verbs.add(name);//add name of the chunk

                }}

                if(newline.contains("reftype='E'"))
                {
                    Getvalues gv=new Getvalues();
                    ArrayList<String> refs=gv.getReferent(newline);//get referents of the anaphore in a list
                    String ref=refs.get(refs.size()-1);//get the closest referent
                    if(ref.contains("%"))//if the closest referent lies in another sentence
                    {
                        //retrieve sentence id of the closest referent
                    int i1=ref.indexOf('%')+1;
                    int i2=ref.indexOf('%',i1);
                    String sid=ref.substring(i1,i2);
                    refidint=Integer.parseInt(sid);

                    int tempdis=currentidint-refidint;//calculate sentence distance b/w anaphore and its closest referent

                    if(!dis.containsKey(tempdis))
                        dis.put(tempdis, 1);
                    else
                    {
                        int x=dis.get(tempdis);
                        x++;
                        dis.put(tempdis,x);
                    }
                    //put distance and its count into the hashmap
                    }
                    else//if closest referent referent lies in the same sentence
                    {

                        if(verbs.contains(ref))//if closest verb lies in the sentence verbs observed before the anaphora
                        {
                        //sentence distance=0
                        if(!dis.containsKey(0))
                            dis.put(0, 1);
                        else
                        {
                        int x=dis.get(0);
                        x++;
                        dis.put(0,x);
                        }
                        }
                   else//cataphora
                    postreferents++;

                    }
                }
                newline=br.readLine();
            }
        }
        System.out.println(dis);
        System.out.println("no. of cataphoras:"+postreferents);

    }

}
