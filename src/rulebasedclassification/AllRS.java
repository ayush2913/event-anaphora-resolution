/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package event;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author dell
 */
public class AllRS {
    public static void main(String args[]) throws FileNotFoundException, IOException
    {
        int totalevents=0,rsevents=0,j=0;
        String fname,newline,start = null,name = null,yahline,nameofchunk = null,yahstart,sentenceid = null,sidofyah=null;
        boolean yah=false,yahandki=false;
        File directory=new File("unannotated");
        File files[]=directory.listFiles();

        for(int i=0;i<files.length;i++)
        {
            String lines[]=null;
             fname=files[i]+"";
                 FileReader fr=new FileReader(fname);
                 BufferedReader br = new BufferedReader(fr);
                 newline=br.readLine();
                 while(newline!=null)
                 {
                     if(newline.contains("<Sentence id="))
                {
                   yah=false;
                   yahandki=false;//make existance of "yah" and "ki" rs relation as false
                    int ind=newline.indexOf("<Sentence id=")+14;
                    int x=newline.indexOf('\"', ind);
                    sentenceid=newline.substring(ind, x);
                }
                     if(newline.contains("(("))
               {
                        Getvalues gv=new Getvalues();
                    start=newline;
                    name=gv.getName(newline);
                     }
                      if(newline.contains("ref='UNK'") && newline.contains("af='यह,"))
               {
                    totalevents++;
               }
               if(newline.contains("	यह	") && newline.contains("ref='UNK'") && !newline.contains("DEM"))
               {
                    
                  yahstart=start;//store the chunk line of "yah"
                   nameofchunk=name;//store name of the chunk
                   sidofyah=sentenceid;//store sentence id of "yah"
                   yahline=newline;//store the line which contains anaphor yah
                   yah=true;

               }
                     if(newline.contains("	कि	") && yah)
               {
                         
                 if(start.contains("drel='rs:"))
                 {

                     int ind=start.indexOf("drel='rs:")+9;

                     String reltn=start.substring(ind,start.indexOf('\'', ind));
                     
                     if(sentenceid.equals(sidofyah) && reltn.equals(nameofchunk))//check for drel=rs with the yah chunk
                     {
                         Getvalues gv=new Getvalues();
                         String kiname=gv.getName(start);
                        
                         rsevents++;

                         yahandki=true;//make yahandki true to denote that rs relation exists in the current sentence
                     }
                 }
               }
                     newline=br.readLine();
                     }
                 }
                 System.out.println("RS events:"+rsevents);
                 System.out.println("Precision:"+(float)(rsevents)*100/(rsevents)+"%");
                 float recall= (float)rsevents*100/totalevents;
                 System.out.println("Recall:"+recall+"%");
        }

    }


