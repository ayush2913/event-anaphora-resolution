package machineresolution;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import tools.AllowedDrel;
import weka.classifiers.meta.FilteredClassifier;

/* This class takes in the following arguments : 
 * 1. An arraylist of all instances required for training
 * 2. A string containing path to the arff file to be used for training
 * 3. A string containing path to the arff structure file to be used during the testing
 * 4. A hashmap of booleans that indicates the attributes that are to be included
 * 
 * The class trains the machine using the arff and creates an object of the FilteredClassifier
 * and stores it as "resolution_model.model" in the folder "model"
 * 
 * The FilteredClassifier is also public and can be accessed directly
 */

public class Train 
{
	public FilteredClassifier fc = new FilteredClassifier();
	
	public Train(ArrayList<HashMap<String, String>> trainList, String pathToArff, String pathStr, HashMap<String, Boolean> attribs) throws Exception
	{
		FileOutputStream fos1 = new FileOutputStream(pathToArff);
		FileOutputStream fos2 = new FileOutputStream(pathStr);
		
		OutputStreamWriter osw1 = new OutputStreamWriter(fos1);
		OutputStreamWriter osw2 = new OutputStreamWriter(fos2);
		
		osw1.write("@relation anaphora-resolution\n\n");
		osw2.write("@relation anaphora-resolution\n\n");
		
		osw1.write("@attribute anaphora string\n");
		osw2.write("@attribute anaphora string\n");
		
		if(attribs.get("ana_root")) 
			{
				osw1.write("@attribute ana_root string\n");
				osw2.write("@attribute ana_root string\n");
			}
		
		if(attribs.get("ana_drel")) 
			{
				osw1.write("@attribute ana_drel {");
				osw2.write("@attribute ana_drel {");
				
				ArrayList<String> al = AllowedDrel.get();
				for(int j = 0; j < al.size(); j++)
				{
					osw1.write(al.get(j) + ",");
					osw2.write(al.get(j) + ",");
				}
				osw1.write("others}\n");
				osw2.write("others}\n");
			}

		if(attribs.get("ana_semprop")) 
			{
				osw1.write("@attribute ana_semprop {h,in,null,rest}\n");
				osw2.write("@attribute ana_semprop {h,in,null,rest}\n");
			}
		
		if(attribs.get("ana_next")) 
			{
				osw1.write("@attribute ana_next string\n");
				osw2.write("@attribute ana_next string\n");
			}
		
		if(attribs.get("ana_numEle")) 
		{
			osw1.write("@attribute ana_numEle numeric\n");
			osw2.write("@attribute ana_numEle numeric\n");
		}
	
		if(attribs.get("ana_numChild")) 
		{
			osw1.write("@attribute ana_numChild numeric\n");
			osw2.write("@attribute ana_numChild numeric\n");
		}
	
		if(attribs.get("ana_gender")) 
		{
			osw1.write("@attribute ana_gender {any,f,m,null}\n");
			osw2.write("@attribute ana_gender {any,f,m,null}\n");
		}
		
		if(attribs.get("ana_num")) 
		{
			osw1.write("@attribute ana_num {any,null,pl,sg}\n");
			osw2.write("@attribute ana_num {any,null,pl,sg}\n");
		}
		
		if(attribs.get("ana_case")) 
			{
				osw1.write("@attribute ana_case {any,d,null,o}\n");
				osw2.write("@attribute ana_case {any,d,null,o}\n");
			}
		
		if(attribs.get("ana_person")) 
		{
			osw1.write("@attribute ana_person {1,2,2h,3,3h,any,null}\n");
			osw2.write("@attribute ana_person {1,2,2h,3,3h,any,null}\n");
		}
	
		
		if(attribs.get("ana_TAM")) 
			{
				osw1.write("@attribute ana_TAM {0,null,का,के,को,ने,में,से}\n");
				osw2.write("@attribute ana_TAM {0,null,का,के,को,ने,में,से}\n");
			}
		
		
		if(attribs.get("ref_word")) 
		{
			osw1.write("@attribute ref_word string\n");
			osw2.write("@attribute ref_word string\n");
		}
		
		if(attribs.get("ref_root")) 
		{
			osw1.write("@attribute ref_root string\n");
			osw2.write("@attribute ref_root string\n");
		}
	
		if(attribs.get("ref_drel")) 
		{
			osw1.write("@attribute ref_drel {");
			osw2.write("@attribute ref_drel {");
			
			ArrayList<String> al = AllowedDrel.get();
			for(int j = 0; j < al.size(); j++)
			{
				osw1.write(al.get(j) + ",");
				osw2.write(al.get(j) + ",");
			}
			osw1.write("others}\n");
			osw2.write("others}\n");
		}

		if(attribs.get("ref_semprop")) 
		{
			osw1.write("@attribute ref_semprop {h,in,null,rest}\n");
			osw2.write("@attribute ref_semprop {h,in,null,rest}\n");
		}
	
		if(attribs.get("ref_numEle")) 
		{
			osw1.write("@attribute ref_numEle numeric\n");
			osw2.write("@attribute ref_numEle numeric\n");
		}

		if(attribs.get("ref_numChild")) 
		{
			osw1.write("@attribute ref_numChild numeric\n");
			osw2.write("@attribute ref_numChild numeric\n");
		}

		if(attribs.get("ref_gender")) 
		{
			osw1.write("@attribute ref_gender {any,f,m,null}\n");
			osw2.write("@attribute ref_gender {any,f,m,null}\n");
		}
	
		if(attribs.get("ref_num")) 
		{
			osw1.write("@attribute ref_num {any,null,pl,sg}\n");
			osw2.write("@attribute ref_num {any,null,pl,sg}\n");
		}
	
		if(attribs.get("ref_case")) 
		{
			osw1.write("@attribute ref_case {any,d,null,o}\n");
			osw2.write("@attribute ref_case {any,d,null,o}\n");
		}
	
		if(attribs.get("ref_person")) 
		{
			osw1.write("@attribute ref_person {1,2,2h,3,3h,any,null}\n");
			osw2.write("@attribute ref_person {1,2,2h,3,3h,any,null}\n");
		}

	
		if(attribs.get("ref_TAM")) 
		{
			osw1.write("@attribute ref_TAM string\n");
			osw2.write("@attribute ref_TAM string\n");
		}
		
		if(attribs.get("sent_dis")) 
		{
			osw1.write("@attribute sent_dis numeric\n");
			osw2.write("@attribute sent_dis numeric\n");
		}
		
		if(attribs.get("verb_dis")) 
		{
			osw1.write("@attribute verb_dis numeric\n");
			osw2.write("@attribute verb_dis numeric\n");
		}
		
		osw1.write("@attribute class {true, false}\n");
		osw2.write("@attribute class {true, false}\n");
		
		osw1.write("\n@data\n");
		osw2.write("\n@data\n");
		
		osw2.flush();
		osw2.close();
		
		ArrayList<String> allowedDrel = AllowedDrel.get();
		
		for(int i = 0; i < trainList.size(); i++)
		{
			HashMap<String, String> instance = trainList.get(i);
			
			osw1.write(instance.get("anaphora") + ",");
			if(attribs.get("ana_root")) osw1.write(instance.get("ana_root") + ",");
			
			if(attribs.get("ana_drel")) 
				{
				if(allowedDrel.contains(instance.get("ana_drel"))) osw1.write(instance.get("ana_drel") + ",");
				else	osw1.write("others" + ",");
				}
			
			if(attribs.get("ana_semprop")) osw1.write(instance.get("ana_semprop") + ",");
			if(attribs.get("ana_next")) osw1.write(instance.get("ana_next") + ",");
			if(attribs.get("ana_numEle")) osw1.write(instance.get("ana_numEle") + ",");
			if(attribs.get("ana_numChild")) osw1.write(instance.get("ana_numChild") + ",");
			if(attribs.get("ana_gender")) osw1.write(instance.get("ana_gender") + ",");
			if(attribs.get("ana_num")) osw1.write(instance.get("ana_num") + ",");
			if(attribs.get("ana_case")) osw1.write(instance.get("ana_case") + ",");
			if(attribs.get("ana_person")) osw1.write(instance.get("ana_person") + ",");
			if(attribs.get("ana_TAM")) osw1.write(instance.get("ana_TAM") + ",");
			
			if(attribs.get("ref_word")) osw1.write(instance.get("ref_word") + ",");
			if(attribs.get("ref_root")) osw1.write(instance.get("ref_root") + ",");
			if(attribs.get("ref_drel")) 
			{
				if(allowedDrel.contains(instance.get("ref_drel"))) osw1.write(instance.get("ref_drel") + ",");
				else	osw1.write("others" + ",");
			}
			
			if(attribs.get("ref_semprop")) osw1.write(instance.get("ref_semprop") + ",");
			if(attribs.get("ref_numEle")) osw1.write(instance.get("ref_numEle") + ",");
			if(attribs.get("ref_numChild")) osw1.write(instance.get("ref_numChild") + ",");
			if(attribs.get("ref_gender")) osw1.write(instance.get("ref_gender") + ",");
			if(attribs.get("ref_num")) osw1.write(instance.get("ref_num") + ",");
			if(attribs.get("ref_case")) 
			{
				String ref_case = instance.get("ref_case");
				if(ref_case != null && ref_case.equals("0")) osw1.write("o,");
				else osw1.write( ref_case + ",");
			}
			
			if(attribs.get("ref_person")) osw1.write(instance.get("ref_person") + ",");
			if(attribs.get("ref_TAM")) osw1.write(instance.get("red_TAM") + ",");
			
			if(attribs.get("sent_dis")) osw1.write(instance.get("sent_dis") + ",");
			if(attribs.get("verb_dis")) osw1.write(instance.get("verb_dis") + ",");
			
			osw1.write(instance.get("class") + "\n");
			
		}
		osw1.flush();
		osw1.close();
		
		MachineLearn ml = new MachineLearn(pathToArff);
		fc = ml.fc;
	}
}