package tools;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Each node contains the following information.
 * 
 * a. Name of chunk - String
 * b. Parent - String
 * c. POS Tag of chunk - String
 * d. Semprop of chunk - String
 * e. Sentence type of chunk in case of root chunk - String
 * f. Children - ArrayList<String>
 * g. Words present in the chunk - String[]
 * h. Features of each Word - ArrayList<HashMap<String, String>>
 * i. Position of Chunk - int
 * 
 * Features of each word are : 
 * a. word
 * b. word type
 * c. word root
 * d. category
 * e. gender
 * f. number
 * g. person
 * h. case
 * i. TAM
 */
public class Node {

	private String name;
	private String parent;
	private ArrayList<String> children = new ArrayList<String>();
	
	private String drel;
	private String pos_tag;
	private String semprop;
	private String stype;
	private String voicetype;
	
	private int numEle;
	private int posn;
	private String[] wordlist;
	
	private ArrayList<HashMap<String,String>> word_features = new ArrayList<HashMap<String,String>>();
	
	public void setName(String s) {
		name = s;
	}
	public String getName() {
		return name;
	}

	public void setParent(String s) {
		parent = s;
	}
	public String getParent() {
		return parent;
	}

	public void setChildren(ArrayList<String> al) {
		children = al;
	}
	public ArrayList<String> getChildren() {
		return children;
	}

	public void setDrel(String s) {
		drel = s;
	}
	public String getDrel() {
		return drel;
	}
	
	public void setPOS(String s) {
		pos_tag = s;
	}
	public String getPOS() {
		return pos_tag;
	}
	
	public void setSemprop(String s) 
	{
		semprop = s;
	}
	
	public String getSemprop() {
		return semprop;
	}
	
	public void setPosn(int n) {
		posn = n;
	}
	public int getPosn() {
		return posn;
	}
	
	public void setStype(String s) {
		stype = s;
	}
	public String getStype() {
		return stype;
	}
	
	public void setVoicetype(String s) {
		voicetype = s;
	}
	public String getVoicetype() {
		return voicetype;
	}
	
	public void setNumEle(int i ) {
		numEle = i;
	}
	public int getNumEle() {
		return numEle;
	}
	
	public void setWordlist(String[] s)
	{
		wordlist = s;
	}
	public String[] getWordlist()
	{
		return wordlist;
	}
	
	public void setWordFeatures(ArrayList<HashMap<String,String>> al)
	{
		word_features = al;
	}
	public ArrayList<HashMap<String,String>> getWordFeatures()
	{
		return word_features;
	}
	
	public HashMap<String, String> getAnaphora()
	{
		for(int i = 0; i < word_features.size(); i++)
		{
			HashMap<String, String> hm = word_features.get(i);
			if(hm.get("type").equals("PRP"))
				return hm;
		}
		return null;
	}
	
	public HashMap<String, String> getNext()
	{
		for(int i = 0; i < word_features.size(); i++)
		{
			HashMap<String, String> hm = word_features.get(i);
			if(hm.get("type").equals("PRP"))
			{
				if(i != (word_features.size() - 1))
				return word_features.get(i + 1);
			}
		}
		return null;
	}
	
	public HashMap<String, String> getWordOfType(String type)
	{
		for(int i = 0; i < word_features.size(); i++)
		{
			HashMap<String, String> hm = word_features.get(i);
			if(hm.get("type").equals(type))
			{
				return word_features.get(i);
			}
		}
		return null;
	}
}
