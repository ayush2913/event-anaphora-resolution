package tools;

import java.util.ArrayList;

public class AllowedDrel 
{
	public static ArrayList<String> get() 
	{
		ArrayList<String> allowedDrel = new ArrayList<String>();
		//k2,k1,k7t,k7,vmod,sent-adv,r6,rt,rh,k1s,r6-k2,
		allowedDrel.add("k2");
		allowedDrel.add("k7t");
		allowedDrel.add("k7");
		allowedDrel.add("rh");
		allowedDrel.add("sent-adv");
		allowedDrel.add("vmod");
		allowedDrel.add("rt");
		allowedDrel.add("k1s");
		allowedDrel.add("r6");
		allowedDrel.add("r6-k2");
		
		allowedDrel.add("k7a");
		allowedDrel.add("adv");
		allowedDrel.add("k4");
		allowedDrel.add("k7p");
		allowedDrel.add("k4a");
		allowedDrel.add("k5");
		allowedDrel.add("rd");
		allowedDrel.add("nmod__emph");
		allowedDrel.add("k2p");
		allowedDrel.add("ccof");
		allowedDrel.add("jjmod");
		allowedDrel.add("ras-k2");
		
		return allowedDrel;
	}
}
