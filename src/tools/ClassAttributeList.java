package tools;

import java.util.HashMap;

public class ClassAttributeList 
{
	public static HashMap<String, Boolean> get() 
	{
		HashMap<String, Boolean> attrib = new HashMap<String, Boolean>();

		attrib.put("isAlone", true);
		//attrib.put("isAlone", false);

		//attrib.put("noChild", true);
		attrib.put("noChild", false);

		attrib.put("drel", true);
		//attrib.put("drel", false);

		//attrib.put("semprop", true);
		attrib.put("semprop", false);

		attrib.put("next_word", true);
		//attrib.put("next_word", false);

		//attrib.put("case", true);
		attrib.put("case", false);

		//attrib.put("num", true);
		attrib.put("num", false);

		//attrib.put("gender", true);
		attrib.put("gender", false);

		//attrib.put("TAM", true);
		attrib.put("TAM", false);

		return attrib;
	}
}
