package tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Tree 
{
	
	
	public Tree(HashMap<String, Node> nMap)
	{
		Set<String> keys = nMap.keySet();
		
		PosnMap = new HashMap<Integer, Node>();
		
		Iterator<String> it = keys.iterator();
		while(it.hasNext())
		{
			String s = it.next();
			Node n = nMap.get(s);
			if(n.getParent() == null)
				{ 
				root = n;
				break;
				}
		}
		
		Iterator<String> iter = keys.iterator();
		while(iter.hasNext())
		{
			String s = iter.next();
			Keys.add(s);
			Node n = nMap.get(s);
			PosnMap.put(n.getPosn(), n);
		}
		
		NodeMap = nMap;
	}
	
	private int sentence_id = 0;
	private Node root = new Node();
	private HashMap<String, Node> NodeMap;
	private HashMap<Integer, Node> PosnMap;
	private ArrayList<String> Keys = new ArrayList<String>();
	
	public int getSentenceID()
	{
		return sentence_id;
	}
	public void setSentenceID(int id)
	{
		sentence_id = id;
	}
	
	public Node getRoot()
	{
		return root;
	}
	
	public HashMap<String, Node> getNodeMap()
	{
		return NodeMap;
	}
	
	public HashMap<Integer, Node> getPosnMap()
	{
		return PosnMap;
	}
	
	public ArrayList<String> getKeys()
	{
		return Keys;
	}
	
	public Node getNode(String name)
	{
		return NodeMap.get(name);
	}
	
	public Node getNode(int posn)
	{
		return PosnMap.get(posn);
	}
	/* Using this method, a paticular node can be found containing a particular value of a property.
	 * Only the first node with matching value for given property is returned.
	 * The list of available properties to search upon is as follows :
	 * 1. name
	 * 2. parent
	 * 3. drel
	 * 4. pos_tag
	 * 5. semprop
	 * 6. stype
	 * 7. voicetype
	 * 8. numEle
	 * 9. word
	 * 10. root
	*/ 
	public Node find(String prop, String value)
	{
		Set<String> keys = NodeMap.keySet();
		Iterator<String> it = keys.iterator();
		while(it.hasNext())
		{
			String s = it.next();
			Node n = NodeMap.get(s);
			if(prop.equals("name")) if(n.getName().equals(value)) return n;
			if(prop.equals("parent")) if(n.getParent().equals(value)) return n;
			if(prop.equals("drel")) if(n.getDrel().equals(value)) return n;
			if(prop.equals("pos_tag")) if(n.getPOS().equals(value)) return n;
			if(prop.equals("semprop")) if(n.getSemprop().equals(value)) return n;
			if(prop.equals("stype")) if(n.getStype().equals(value)) return n;
			if(prop.equals("voicetype")) if(n.getVoicetype().equals(value)) return n;
			if(prop.equals("numEle")) if(n.getNumEle() == Integer.valueOf(value)) return n;
			
			if(prop.equals("word"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("word") != null)
					{
						if(hm.get("word").equals(value)) return n; 
					}
				}
			}
			
			if(prop.equals("root"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("root") != null)
					{
						if(hm.get("root").equals(value)) return n; 
					}
				}
			}
			
			if(prop.equals("reftype"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("reftype") != null)
					{
						if(hm.get("reftype").equals(value)) return n;
					}
				}
			}
		}
		return null;
	}
	
	
	
	/* Using this method, all nodes can be found containing a particular value of a property.
	 * The list of available properties to search upon is as follows :
	 * 1. name
	 * 2. parent
	 * 3. drel
	 * 4. pos_tag
	 * 5. semprop
	 * 6. stype
	 * 7. voicetype
	 * 8. numEle
	 * 9. word
	 * 10. root
	*/ 
	
	public ArrayList<Node> findAll(String prop, String value)
	{
		ArrayList<Node> nodeList = new ArrayList<Node>();
		
		Set<String> keys = NodeMap.keySet();
		Iterator<String> it = keys.iterator();
		while(it.hasNext())
		{
			String s = it.next();
			Node n = NodeMap.get(s);
			if(prop.equals("name")) if(n.getName().equals(value)) nodeList.add(n);
			if(prop.equals("parent")) if(n.getParent().equals(value)) nodeList.add(n);
			if(prop.equals("drel")) if(n.getDrel().equals(value)) nodeList.add(n);
			if(prop.equals("pos_tag")) if(n.getPOS().equals(value)) nodeList.add(n);
			if(prop.equals("semprop")) if(n.getSemprop().equals(value)) nodeList.add(n);
			if(prop.equals("stype")) if(n.getStype().equals(value)) nodeList.add(n);
			if(prop.equals("voicetype")) if(n.getVoicetype().equals(value)) nodeList.add(n);
			if(prop.equals("numEle")) if(n.getNumEle() == Integer.valueOf(value)) nodeList.add(n);
			
			if(prop.equals("word"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("word") != null)
					{
						if(hm.get("word").equals(value)) nodeList.add(n); 
					}
				}
			}
			
			if(prop.equals("root"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					//System.out.println(hm);
					if(hm.get("root") != null)
					{
						if(hm.get("root").equals(value)) nodeList.add(n); 
				
					}
				}
			}
			
			if(prop.equals("reftype"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("reftype") != null)
					{
						if(hm.get("reftype").equals(value)) nodeList.add(n);
					}
				}
			}
		}
		
		return nodeList;
	}
	
	public ArrayList<Node> findAllContaining(String prop, String value)
	{
		ArrayList<Node> nodeList = new ArrayList<Node>();
		
		Set<String> keys = NodeMap.keySet();
		Iterator<String> it = keys.iterator();
		while(it.hasNext())
		{
			String s = it.next();
			Node n = NodeMap.get(s);
			if(prop.equals("name")) if(n.getName().contains(value)) nodeList.add(n);
			if(prop.equals("parent")) if(n.getParent().contains(value)) nodeList.add(n);
			if(prop.equals("drel")) if(n.getDrel().contains(value)) nodeList.add(n);
			if(prop.equals("pos_tag")) if(n.getPOS().contains(value)) nodeList.add(n);
			if(prop.equals("semprop")) if(n.getSemprop().contains(value)) nodeList.add(n);
			if(prop.equals("stype")) if(n.getStype().contains(value)) nodeList.add(n);
			if(prop.equals("voicetype")) if(n.getVoicetype().contains(value)) nodeList.add(n);
			if(prop.equals("numEle")) if(n.getNumEle() == Integer.valueOf(value)) nodeList.add(n);
			
			if(prop.equals("word"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("word") != null)
					{
						if(hm.get("word").equals(value)) nodeList.add(n); 
					}
				}
			}
			
			if(prop.equals("root"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					//System.out.println(hm);
					if(hm.get("root") != null)
					{
						if(hm.get("root").equals(value)) nodeList.add(n); 
				
					}
				}
			}
			
			if(prop.equals("reftype"))
			{
				ArrayList<HashMap<String,String>> al = n.getWordFeatures();
				for(int i = 0; i < al.size(); i++)
				{
					HashMap<String,String> hm = al.get(i);
					if(hm.get("reftype") != null)
					{
						if(hm.get("reftype").equals(value)) nodeList.add(n);
					}
				}
			}
		}
		
		return nodeList;
	}
}
