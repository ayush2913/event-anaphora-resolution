package tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/* Takes in a chunk as an ArrayList and returns a Node containing the following information
 * 
 * a. Name of chunk - String
 * b. Parent - String
 * c. POS Tag of chunk - String
 * d. Semprop of chunk - String
 * e. Sentence type of chunk in case of root chunk - String
 * f. Children - ArrayList<String>
 * g. Words present in the chunk - String[]
 * h. Features of each Word - ArrayList<HashMap<String, String>>
 * 
 */

public class MakeNode 
{
	public Node node;
	
	public MakeNode(ArrayList<String> al) 
	{
		node = new Node();
		
		int numEle = al.size() - 1;
		node.setNumEle(numEle);
		
		ArrayList<String> x = new ArrayList<String>();
		String drel = null, parent = null;
		String semprop = null;
		String stype = null;
		String voicetype = null;
		
		String chunk = al.get(0);
		Scanner sc = new Scanner(chunk);
		sc.useDelimiter("\t<|='|'>|' |:|\t");
		while(sc.hasNext()) 
		{
			String s = sc.next();
			x.add(s);
			if(s.equals("drel")) {drel = sc.next(); parent = sc.next();}
			if(s.equals("semprop")) semprop = sc.next();
			if(s.equals("stype")) stype = sc.next();
			if(s.equals("voicetype")) voicetype = sc.next();
		}
		sc.close();
		
		node.setDrel(drel);
		node.setSemprop(semprop);
		node.setVoicetype(voicetype);
		node.setStype(stype);
		node.setParent(parent);
		
		node.setPosn(Integer.valueOf(x.get(0)));
		node.setName(x.get(4));
		node.setPOS(x.get(2));
		
		ArrayList<HashMap<String,String>> wordfeats = new ArrayList<HashMap<String,String>>();
		
		for(int i = 1; i < al.size(); i++)
		{
			String s = al.get(i);
			HashMap<String,String> hm = new HashMap<String,String>();
			ArrayList<String> y = new ArrayList<String>();
			
			Scanner scan = new Scanner(s);
			scan.useDelimiter("\t<|'>|\">|=\"|='|' |,|\t");
			
			while(scan.hasNext())
			{
				String z = scan.next();
				y.add(z);
			}
			scan.close();
			
			// for cases of "NULL"
			if(y.size() < 10)
			{
				hm.put("word", y.get(1));
				hm.put("type", y.get(2));
				hm.put("root", null);
				hm.put("cat", null);
				hm.put("gender", null);
				hm.put("num", null);
				hm.put("person", null);
				hm.put("case", null);
				hm.put("TAM", null);
				hm.put("ref", null);
				hm.put("refmod", null);
				hm.put("reftype", null);
			}
			else
			{
				
				hm.put("word", y.get(1));
				hm.put("type", y.get(2));
				hm.put("root", y.get(4));
				if(y.get(5).equals("")) hm.put("cat", null);
				else hm.put("cat", y.get(5));
				
				if(y.get(6).equals("")) hm.put("gender", null);
				else hm.put("gender", y.get(6));
				
				if(y.get(7).equals("")) hm.put("num", null);
				else hm.put("num", y.get(7));
				
				if(y.get(8).equals("")) hm.put("person", null);
				else hm.put("person", y.get(8));
				
				if(y.get(9).equals("")) hm.put("case", null);
				else hm.put("case", y.get(9));
				
				if(y.get(10).equals("")) hm.put("TAM", null);
				else hm.put("TAM", y.get(10));
				
				if(y.contains("ref")) 
					hm.put("ref", y.get(y.indexOf("ref") + 1));
				else 
					hm.put("ref", null);
				
				if(y.contains("refmod")) 
					hm.put("refmod",y.get(y.indexOf("refmod") + 1));
				else
					hm.put("refmod", null);
				
				if(y.contains("reftype")) 
					hm.put("reftype", y.get(y.indexOf("reftype") + 1));
				else 
					hm.put("reftype", null);
			}
			wordfeats.add(hm);
		}
		
		node.setWordFeatures(wordfeats);
		
		String[] wordlist = new String[numEle];
		for(int i = 0; i < numEle; i++)
		{
			wordlist[i] = wordfeats.get(i).get("word");
		}
		
		node.setWordlist(wordlist);
		
		//return n;
	}
}
