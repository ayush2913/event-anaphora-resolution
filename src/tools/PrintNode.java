package tools;

public class PrintNode 
{
	public static void print(Node node)
	{
		/*
		System.out.print("Name");
		System.out.print("\t");
		System.out.print("Drel");
		System.out.print("\t");
		System.out.print("Parent");
		System.out.print("\t");
		System.out.print("POS");
		System.out.print("\t");
		System.out.print("Semprop");
		System.out.print("\t");
		System.out.print("Stype");
		System.out.print("\t");
		System.out.print("Vtype");
		System.out.print("\t");
		System.out.print("Child");
		System.out.print("\t");
		System.out.print("NumEle");
		System.out.print("\t");
		System.out.print("Wordlist");
		System.out.print("\t");
		System.out.print("WordFeats");
		System.out.print("\t");
		System.out.println();
		*/
		
		
		System.out.print(node.getName());
		System.out.print("\t");
		System.out.print(node.getDrel());
		System.out.print("\t");
		System.out.print(node.getParent());
		System.out.print("\t");
		System.out.print(node.getPOS());
		System.out.print("\t");
		System.out.print(node.getSemprop());
		System.out.print("\t");
		System.out.print(node.getStype());
		System.out.print("\t");
		System.out.print(node.getVoicetype());
		System.out.print("\t");
		System.out.print(node.getChildren());
		System.out.print("\t");
		System.out.print(node.getNumEle());
		System.out.print("\t");
		System.out.print(node.getWordlist());
		System.out.print("\t");
		System.out.print(node.getWordFeatures());
		System.out.print("\t");
		System.out.println();
	}
}
