/** 
 * Project : Event Anaphora Resolution
 * 
 * Project Mentor : Praveen Dhakwale
 * 
 * Author : Venkatesh Shukla, Anjali Jain
 * 
 * Language Technology and Research Center , Internatinal Institute of Information Technology
 * 
 * This Package contains the classes used in the main code for various purposes
 * 
 * The classes are as follows
 * 1. Tree : A tree data structure implemented for java
 * 2. Node : Node of the tree
 * 3. MakeNode : Takes input as an ArrayList<String> containing all the strings of a chunk and returns a Node containing all the information.
 * 4. MakeTree : Takes input as an ArrayList<String> containing the parsed sentence and returns a Tree of the sentence
 * 5. ProcessFile : Takes input as a file to be processed and returns a HashMap containing sentence_id and the tree of each sentence.
 * 6. ClassAttributeList : This class decides the attributes used in the machine learning for anaphora classifier. Returns a HashMap<String, Boolean>.
 * 		Make the attributes which need to be included as true and the ones to be excluded as false.
 * 7. ResolveAttributeList : This class decides the attributes used in the machine learning for anaphora classifier. Returns a HashMap<String, Boolean>.
 * 		Make the attributes which need to be included as true and the ones to be excluded as false.
 * 8. AllowedDrel : This class returns a HashMap<String, Boolean>. It tells the classes which drels to be included in arff. The ones to be included are to be set as true. The rest are marked false.
 * 9. PrintNode : Prints the different values present in a node. Takes Node as argument.
 * 10. PrintAllNode : Prints all nodes present in a sentence. Takes HashMap<String, Node> as argument.
 * 11. PrintInstanceList : Prints all the features present in a list of instances. Takes in ArrayList<HashMap<String, String>> as argument.
 **/
package tools;