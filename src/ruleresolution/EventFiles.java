package ruleresolution;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

import tools.Node;
import tools.ProcessFile;
import tools.Tree;

public class EventFiles 
{
	// this class prints out the number of anaphora and their sentence id
	public static void main(String[] args) 
	{
		// All the files are present in this directory
		String path = "annotated";
		File f1 = new File(path);
		
		String[] fileList = f1.list();
		
		TreeMap<String, ArrayList<Integer>> ts = new TreeMap<String, ArrayList<Integer>>();
		
		for(int i = 0; i < fileList.length; i++)
		{
			// For a single file
			String filename = fileList[i];
			//System.out.println(filename);
			File f2 = new File(f1.getAbsoluteFile() + "/" + filename);
			ProcessFile pf = new ProcessFile(f2);
			
			ArrayList<Integer> keys = pf.Keys;
			HashMap<Integer, Tree> oneFile = pf.ProcessedFile;
			ArrayList<Integer> sentlist = new ArrayList<Integer>();
			for(int j = 0; j < keys.size(); j++)
			{
				// for a single sentence
				int sent_id = keys.get(j);
				//System.out.println("Sentence :" + sent_id);
				Tree stree = oneFile.get(sent_id);
				// Analyse the treee to get all the features required.
				
				ArrayList<Node> nodeList = stree.findAll("reftype", "E");
				
				if(nodeList.size() > 0) 
					{
						sentlist.add(sent_id);
					}
				
			}
			ts.put(filename, sentlist);
		}
		
		for(int i = 0; i < fileList.length; i++)
		{
			// For a single file
			String filename = fileList[i];
			Scanner scan = new Scanner(filename);
			scan.useDelimiter("_");
			scan.next();
			scan.next();
			String filenum = scan.next();
			scan.close();
			System.out.println(filenum + "\t" + ts.get(filename));
		}
	}

}
