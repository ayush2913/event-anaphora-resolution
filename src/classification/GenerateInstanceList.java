package classification;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import tools.Node;
import tools.PrintNode;
import tools.ProcessFile;
import tools.Tree;

public class GenerateInstanceList 
{
	public ArrayList<HashMap<String, String>> instanceList = new ArrayList<HashMap<String, String>>();
	
	public GenerateInstanceList(String path)
	{
		// All the files are present in this directory
		File f1 = new File(path);
		
		String[] fileList = f1.list();
		
		for(int i = 0; i < fileList.length; i++)
		{
			// For a single file
			String filename = fileList[i];
			//System.out.println(filename);
			File f2 = new File(f1.getAbsoluteFile() + "/" + filename);
			ProcessFile pf = new ProcessFile(f2);
			
			ArrayList<Integer> keys = pf.Keys;
			HashMap<Integer, Tree> oneFile = pf.ProcessedFile;
			
			for(int j = 0; j < keys.size(); j++)
			{
				// for a single sentence
				int sent_id = keys.get(j);
				//System.out.println("Sentence :" + sent_id);
				Tree stree = oneFile.get(sent_id);
				
				// Analyse the treee to get all the features required.
				
				// Concentrating on those case having "यह" as root of the pronoun
				ArrayList<Node> nodeList = stree.findAll("root", "यह");
				
				ArrayList<Node> filteredNodeList = new ArrayList<Node>();
				
				// Filter the nodeList to get only those cases which are not demonstratives
				for(int k = 0; k < nodeList.size(); k++)
				{
					Node n = nodeList.get(k);
					ArrayList<HashMap<String,String>> al = n.getWordFeatures();
					boolean prp = false;
					for(int l = 0; l < al.size(); l++)
					{
						HashMap<String, String> hm = al.get(l);
						if(hm.get("type").equals("PRP")) 
							{
								//hm.get("ref") != null && 
								if(hm.get("ref").equals("UNK")) continue;
								
								prp = true;
								break;
							}
					}
					
					if(prp) filteredNodeList.add(n);
					else continue;
				}
				
				// All nodes having pronouns with "यह" as root and non-UNK are added in this 
				ArrayList<Node> finalNodeList = new ArrayList<Node>();
				
				// All those pronouns with with "यह" as root and
				// "कि" as their descendant having "rs" dependency relation are added here
				ArrayList<Node> yehkiNodeList = new ArrayList<Node>();
				
				for(int k = 0; k < filteredNodeList.size(); k++)
				{
					boolean yehki = false;
					Node node = filteredNodeList.get(k);
					ArrayList<String> children = node.getChildren();
					for(int l = 0; l < children.size(); l++)
					{
						String s = children.get(l);
						if(stree.getNode(s).getPOS().equals("CCP") && stree.getNode(s).getDrel().equals("rs"))
						{
							yehki = true;
							yehkiNodeList.add(node);
							break;
						}
					}
					if(!yehki)
					{
						finalNodeList.add(node);
					}
				}
				
				// We have a final Node List for a sentence containing those chunks that have pronouns with 
				// "यह" as their roots
				// and not having "कि" as their descendant having "rs" dependancy relation
				//  Do the analysis below
				for(int k = 0; k < finalNodeList.size(); k++)
				{
					// One instance of non Demonstrative Pronoun with "यह" root
					Node node = finalNodeList.get(k);
					//PrintNode.print(node);
					/* We are adding the following information in the instance
					 * 1. Word 2. Alone/Not 3. SingleChild/Not 4. Drel 5. Next Word
					 * 6. Case 7. Num 8. Gender 9. TAM 10. Semprop 11. class
					 * 12. filename 13. Sentence ID 14. Chunk Name
					 */
					HashMap<String, String> instance = new HashMap<String, String>();
					
					if((node.getNumEle() == 1) || (node.getNext() == null))
						{
							instance.put("isAlone","true");
							instance.put("next_word", null);
						}
					else if((node.getNumEle() > 1)) 
						{
							instance.put("isAlone","false");
							instance.put("next_word", node.getNext().get("word"));
						}
					
					if(node.getChildren().size() == 0) instance.put("noChild", "true");
					else  instance.put("noChild", "false");
					
					instance.put("drel", node.getDrel());
					
					instance.put("semprop", node.getSemprop());
					
					instance.put("filename", filename);
					
					instance.put("sentence_id", String.valueOf(sent_id));
					
					instance.put("chunk_name", node.getName());
					
					HashMap<String, String> anaphora = node.getAnaphora();
					
					instance.put("anaphora", anaphora.get("word"));
					
					instance.put("case", anaphora.get("case"));
					
					instance.put("num", anaphora.get("num"));
					
					instance.put("gender", anaphora.get("gender"));
					
					instance.put("TAM", anaphora.get("TAM"));
					
					if(anaphora.get("ref").contains("NP")) instance.put("class", "entity");
					else if(anaphora.get("reftype") != null && anaphora.get("reftype").equals("E")) instance.put("class", "event");
					else continue;
					
					instanceList.add(instance);
				}		
			}
		}
		//PrintInstanceList.print(instanceList);
		// Now we have an ArrayList of all instances of occurrence of Pronoun having "यह" as root, non - UNK and non - DEM and non NULL
	}
}
