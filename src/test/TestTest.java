package test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import tools.ClassAttributeList;
import weka.classifiers.meta.FilteredClassifier;
import classification.GenerateInstanceList;
import classification.Test;

public class TestTest 
{
	public static void main(String[] args) throws Exception
	{
		String pathToModel = "model/classifier_model.model";
		FileInputStream fis = new FileInputStream(pathToModel);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		FilteredClassifier fc = (FilteredClassifier) ois.readObject();
		ois.close();
		

		GenerateInstanceList gil = new GenerateInstanceList("annotated");
		ArrayList<HashMap<String,String>> allInstances = gil.instanceList;
		
		HashMap<String, Boolean> attrib = ClassAttributeList.get();
		
		String pathStr = "arff/classify_structure.arff";
		
		Test test = new Test(allInstances, pathStr, fc, attrib);
	}
}