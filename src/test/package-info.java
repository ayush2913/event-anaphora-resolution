/**
 * Project : Event Anaphora Resolution
 * 
 * Project Mentor : Praveen Dhakwale
 * 
 * Author : Venkatesh Shukla, Anjali Jain
 * 
 * Language Technology and Research Center , Internatinal Institute of Information Technology
 * 
 * This Package contains the classes used in the testing of classes made in other packages.
 * 1. PrintPlainSentences.java -  This class prints out the files as human readable chunked files stored according to their filenum in the folder "simple"
 * 	Those words which are event anaphora
 * 2. TestTest.java - This class check if the Test.java file of package classification works or not.
 * 3. TestTrain.java - This class check if the Train.java file of package classification works or not.
 * 4. TestResoTrain.java - This class checks if Train.java of package machineresolution works or not.
 * 5. TestResoAll.java - This class checks if training and testing of Resolution is happening without error.
 **/
package test;