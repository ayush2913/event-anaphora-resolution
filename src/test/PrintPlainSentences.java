package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/*
 * This class prints out the files as human readable chunked files stored according to their filenum in the folder "simple"
 * Those words which are event anaphora
 */

public class PrintPlainSentences 
{
	public static void main(String[] args) throws Exception
	{
		File f = new File("annotated");
		String[] fileList = f.list();
		int count = 0;
		for(int i = 0; i < fileList.length; i++)
		{
			String fileName = fileList[i];
			
			String fileNum = fileName.split("_")[2];
			System.out.println(fileNum);
			
			FileInputStream fis = new FileInputStream("annotated/" + fileName);
			InputStreamReader isr = new InputStreamReader(fis, "UTF8");
			BufferedReader br = new BufferedReader(isr);
			
			FileOutputStream fos = new FileOutputStream("simple/" + fileNum);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			
			String line;
			int lineNum = 0;
			boolean check = false;
			
			while((line = br.readLine()) != null)
			{
				lineNum++;
				//System.out.printf("%d\t%s\n", lineNum,line);
				
				if(line.length() == 0)
				{
					continue;
				}
				
				else if(line.contains("<Sentence id"))
				{
					check = true;
					String sent_id = line.split("\"|'")[1];
					osw.write(sent_id + ".\t");
					System.out.print(sent_id + ".\t");
				}
				
				else if(line.contains("</Sentence>"))
				{
					check = false;
					osw.write("\n");
					System.out.print("\n");
				}
				
				else if(line.contains("))"))
				{
					osw.write(" )) ");
					System.out.print("))");
				}
				
				else if(check)
				{
					String word = line.split("\t")[1];
					if(line.contains("reftype='E'") && !line.contains("इसलिए"))	
					{
						count++;
						osw.write("\n\n" + word + "\n\n\t\t");
						System.out.print("\n\n" + word + "\n\n\t\t");
					}
					
					else
					{
						osw.write(" " + word + " ");
						System.out.print(" " + word + " ");
					}
				}
			}
			
			br.close();
			osw.flush();
			osw.close();
		}
		System.out.println(count);
	}
}
