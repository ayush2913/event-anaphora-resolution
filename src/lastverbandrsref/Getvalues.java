/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package event;
import java.io.*;
import java.util.*;

/**
 *
 * @author dell
 */
public class Getvalues
 {
    public String getSType(String line)//returns sentence type of the line if present otherwise null
    {
        String stype="null";
        if(line.contains("stype='"))
        {
        int index1=line.indexOf("stype='")+7;
        int index2=line.indexOf("'", index1);
        stype=line.substring(index1,index2);
        }
        //System.out.println("stypr:"+line);
        return stype;
    }
    public String getVoiceType(String line)//returns voice type of the line
    {
        String vt="null";
        if(line.contains("voicetype='"))
        {
        int index1=line.indexOf("voicetype='")+11;
        int index2=line.indexOf("'", index1);
        vt=line.substring(index1,index2);
        }
        //System.out.println("vtype:"+line);
        return vt;
    }
    public String getdrel(String line)//returns drel of the line
    {
        String drel="null";
        //System.out.println(line);
        if(line.contains("drel='"))
        {
        int index=line.indexOf("drel='")+6;
        int index2=line.indexOf(':',index);
        drel=line.substring(index,index2);
        }
 else
     drel="null";
        //System.out.println(drel);
        return drel;
    }
    public String getWord(String line)//returns word
    {
        
        String word=null;
        int index=line.indexOf(9)+1;
        int index2=line.indexOf(9,index);
        word=line.substring(index,index2);
        //System.out.println(word);
        return word;
    }
    public String getTag(String line)//returns pos tag of the line
    {
        String tag=null;
        int index=line.indexOf(9, line.indexOf("((")+3);
        tag=line.substring(line.indexOf("((")+3, index);
        return tag;
    }
    public String getName(String line)//returns name of the line(chunk line)
    {
        String name=null;
        int index=line.indexOf("name='");
        int index2=line.indexOf('\'',index+6);
        name=line.substring(index+6,index2);
        return name;
    }
    public ArrayList<String> getReferent(String line)//returns list of referents
    {
        int comma=0;
        ArrayList<String> referents = new ArrayList<String>();//
        String referent=null;
        int index=line.indexOf("ref='")+5;
        int index1=line.indexOf('\'',index);
        referent=line.substring(index,index1);
        
        //System.out.println(referent);
        index=0;

        while(referent.contains(","))
        {

            int ind=referent.indexOf(',');
            String refpre=referent.substring(0,ind);
            referent=referent.substring(ind+1);
            referents.add(refpre);

            //System.out.print(refpre+","+referent);
            
            comma++;
        }
        referents.add(referent);
        //System.out.println();
        //if(comma>0)
        //System.out.println("\nno of referents:"+(comma+1));
        //System.out.println();
        //System.out.println(referents);
        return referents;
    }

}
